
import { createGlobalStyle } from 'styled-components';

export const StylingResetter = createGlobalStyle`
  *, *:before, *:after {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    
    font-family: -apple-system,
    BlinkMacSystemFont,
    "Segoe UI",
    "Roboto",
    "Oxygen",
    "Ubuntu",
    "Cantarell",
    "Fira Sans",
    "Droid Sans",
    "Helvetica Neue",
    sans-serif;
  }
  button {
    &:active, &:focus, &:focus-within, &:hover, &:visited {
      outline: none;
    }
  }
  body {
    overflow: auto;
  }
  img {
    display: block;
  }
  /* input::placeholder {
    color: lightgray;
    opacity: 1;
  } */
`;

export default { StylingResetter };
