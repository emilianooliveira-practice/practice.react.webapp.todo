
import React from 'react';
import styled from 'styled-components';

import { pointer } from '../../styled/helpers/helpers.styled';

const Task = ({ label, onClick }) => (
  <Wrapper onClick={ onClick }>
    <Label>
      { label }
    </Label>
  </Wrapper>
);

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0 10px;
  height: 40px;
  border: 1px solid dimgray;
  border-radius: 3px;
  margin-bottom: 10px;
  ${ pointer }
`;

const Label = styled.label`
  align-self: center;
`;

export default Task;
