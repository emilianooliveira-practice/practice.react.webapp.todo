
import React from 'react';
import styled from 'styled-components';
import { pointer } from '../../styled/helpers/helpers.styled';

const NewTaskButton = ({ onClick }) => (
  <Wrapper>
    new task
  </Wrapper>
);

const Wrapper = styled.button.attrs({
  type: 'submit',
})`
  padding: 0 10px;
  display: flex;
  justify-content: center;
  height: 100%;
  background: none;
  border-radius: 3px;
  align-self: center;
  text-transform: uppercase;
  border: 1px solid dimgray;
  margin: 0;
  height: 40px;

  ${ pointer }
`;

export default NewTaskButton;
