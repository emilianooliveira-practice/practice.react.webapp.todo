
import React from 'react';
import styled from 'styled-components';

const Title = () => (
  <Wrapper>
    The Amazing TO-DO
  </Wrapper>
);

const Wrapper = styled.h1`
  color: dimgray;
  align-self: center;
  margin-bottom: 50px;
`;

export default Title;
