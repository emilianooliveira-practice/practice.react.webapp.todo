
import React, { useState } from 'react';
import styled, { css } from 'styled-components';

const NewTaskInput = ({ onChange }) => {

  const [ state, setState ] = useState({
    length: 0,
  });

  return (
    <Wrapper
      length={ state.length }
      onChange={ event => {
        const { value } = event.target;
        onChange(value);
        setState(prev => ({
          ...prev,
          length: value.length,
        }));
      } }
    />
  );
};

const Wrapper = styled.input.attrs({
  placeholder: 'Describe your task here...',
})`
  flex-grow: 1;
  width: 1px;
  margin-right: 10px;
  border-radius: 3px;
  border: 1px solid dimgray;
  padding-left: 10px;
  max-height: 40px;
  font-size: 16px;

  ${ ({ length }) => {
    if(length > 2 && length <= 5) {
      return css`
        color: green;
      `;
    } else if(length > 5) {
      return css`
        color: red;
      `;
    }
  } }
`;

export default NewTaskInput;
