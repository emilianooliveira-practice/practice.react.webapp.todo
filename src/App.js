
import React, { Fragment, useState } from 'react';
import styled from 'styled-components';
import { v4 as uuid } from 'uuid';

import { StylingResetter } from './styled/global/global.styled';
import Title from './components/title/Title';
import Task from './components/task/Task';
import NewTaskInput from './components/new-task-input/NewInputTask';
import NewTaskButton from './components/new-task-button/NewTaskButton';

const App = () => {

  const [ state, setState ] = useState({
    tasks: [{
      id: uuid(),
      isChecked: true,
      label: 'Task 1',
      lastUpdate: Date.now(),
    }, {
      id: uuid(),
      isChecked: true,
      label: 'Task 2',
      lastUpdate: Date.now(),
    }, {
      id: uuid(),
      isChecked: true,
      label: 'Task 3',
      lastUpdate: Date.now(),
    }, {
      id: uuid(),
      isChecked: false,
      label: 'Task 4',
      lastUpdate: Date.now(),
    }],
    taskLabel: '',
  });

  const handleTaskClick = id => {
    const tasks = [ ...state.tasks ];
    const index = tasks.findIndex(t => {
      return t.id === id;
    });
    
    tasks[index].isChecked = !tasks[index].isChecked;
    tasks[index].lastUpdate = Date.now();

    setState(prev => ({
      ...prev,
      tasks,
    }));
  };

  const handleFormSubmit = event => {
    event.preventDefault();
    if(!state.taskLabel) alert('Invalid task description.');
    else {
      const tasks = [ ...state.tasks, ];
      tasks.push({
        id: uuid(),
        label: state.taskLabel,
        isChecked: false,
        lastUpdate: Date.now(),
      });
      
      setState(prev => ({
        ...prev,
        tasks,
      }));
    }
  };

  const getSortedTasks = (active = true) => {
    const sorted = state.tasks.filter(t => {
      return active ? !t.isChecked : t.isChecked;
    }).sort((a, b) => b.lastUpdate - a.lastUpdate);

    return sorted;
  };

  return (
    <Fragment>
      <StylingResetter />
      <Wrapper>
        <Title />
        <TaskList>
          { getSortedTasks().map(({id, label }) => (
            <Task 
              key={ id }
              label={ label }
              onClick={ () => handleTaskClick(id) }
            />
          )) }
          
          <NewTaskForm 
            onSubmit={ handleFormSubmit }
          >
            <NewTaskInput
              onChange={ taskLabel => {
                setState(prev => ({
                  ...prev,
                  taskLabel,
                }))
              } }
            />
            <NewTaskButton />
          </NewTaskForm>
        </TaskList>

        <InactiveTaskList>
          <CompletedIndicator>
            Completed Tasks:
          </CompletedIndicator>
          { getSortedTasks(false).map(({id, label }) => (
            <Task 
              key={ id }
              label={ label }
              onClick={ () => handleTaskClick(id) }
            />
          )) }
        </InactiveTaskList>
      </Wrapper>
    </Fragment>
  );
};

const Wrapper = styled.div`
  margin-top: 60px;
  display: flex;
  flex-direction: column;
`;

const TaskList = styled.div`
  align-self: center;
  width: 100%;
  max-width: 400px;
`;

const NewTaskForm = styled.form`
  margin-top: 30px;
  display: flex;
  height: 40px;
`;

const CompletedIndicator = styled.label`
  margin-bottom: 10px;
  display: block;
`;

const InactiveTaskList = styled(TaskList)`
  opacity: 0.3;
  margin-top: 50px;
`;

export default App;
